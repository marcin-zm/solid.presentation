﻿namespace SOLID._3.Dobrze.Contracts
{
    public class Dane
    {
        public string Identyfikator { get; set; }
        public string Nazwa { get; set; }
        public string Opis { get; set; }
        public decimal Cena { get; set; }
        public string Waluta { get; set; }
    }
}
