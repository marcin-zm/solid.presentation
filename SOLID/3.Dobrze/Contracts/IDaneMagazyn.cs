﻿using System.Collections.Generic;

namespace SOLID._3.Dobrze.Contracts
{
    public interface IDaneMagazyn
    {
        void Zapisz(IEnumerable<Dane> lista);
    }
}
