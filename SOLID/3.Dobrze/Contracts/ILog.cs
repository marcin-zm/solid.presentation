﻿namespace SOLID._3.Dobrze.Contracts
{
    public interface ILog
    {
        void LogError(string message, params object[] args);
        void LogInfo(string message, params object[] args);
    }
}
