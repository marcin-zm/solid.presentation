﻿using System.Collections.Generic;

namespace SOLID._3.Dobrze.Contracts
{
    public interface IDaneZrodlo
    {
        IEnumerable<string> DajDane();
    }
}
