﻿using System.Linq;
using SOLID._3.Dobrze.Contracts;

namespace SOLID._3.Dobrze
{
    public class DaneImport
    {
        private readonly IDaneZrodlo _daneZdrodlo;
        private readonly IDaneParser _daneParser;
        private readonly IDaneMagazyn _daneMagazyn;

        public DaneImport( IDaneZrodlo daneZdrodlo, IDaneParser daneParser, IDaneMagazyn daneMagazyn)
        {
            _daneZdrodlo = daneZdrodlo;
            _daneParser = daneParser;
            _daneMagazyn = daneMagazyn;
        }
        public void Importuj()
        {
            var linieZPliku = _daneZdrodlo.DajDane();
            var listaRekordow = _daneParser.ParsujDane(linieZPliku.ToArray());
            _daneMagazyn.Zapisz(listaRekordow);
        }
    }
}
