﻿using System.Collections.Generic;
using SOLID._3.Dobrze.Contracts;

namespace SOLID._3.Dobrze.Implementation
{
    public class DaneParser: IDaneParser
    {
        private readonly IDaneWalidator _daneWalidator;
        private readonly ILog _log;
        public DaneParser(IDaneWalidator daneWalidator, ILog log)
        {
            _daneWalidator = daneWalidator;
            _log = log;
        }

        public IEnumerable<Dane> ParsujDane(string[] linieImportu)
        {
            var listaDane = new List<Dane>(linieImportu.Length);
            int iloscRekordow = 0;
            for (int i = 0; i < linieImportu.Length; i++)
            {
                var line = linieImportu[i];
                var wiersz = line.Split('|');
                if (_daneWalidator.Sprawdz(wiersz, i + 1))
                {
                    listaDane.Add(Parsuj(wiersz));
                    iloscRekordow++;
                }
            }
            _log.LogInfo("Dodano {0} rekordów",iloscRekordow);
            return listaDane;
        }

        private Dane Parsuj(string[] wiersz)
        {
            Dane dane = new Dane()
            {
                Identyfikator = wiersz[0],
                Nazwa = wiersz[1],
                Opis = wiersz[2],
                Cena = decimal.Parse(wiersz[3]),
                Waluta = wiersz[4]
            };
            return dane;
        }
    }
}
