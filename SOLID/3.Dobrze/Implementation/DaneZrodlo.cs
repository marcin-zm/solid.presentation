﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SOLID._3.Dobrze.Contracts;

namespace SOLID._3.Dobrze.Implementation
{
    public class DaneZrodlo : IDaneZrodlo
    {
        private string _plikSciezka;
        private readonly ILog _log;

        public DaneZrodlo(ILog log, string plikSciezka )
        {
            _plikSciezka = plikSciezka;
            _log = log;
        }

        public IEnumerable<string> DajDane()
        {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(_plikSciezka);
            if (stream == null)
            {
                _log.LogInfo("Nie ma pliku!");
                return new string[] {};
            }

            var linieImportu = new List<string>();
            using (var reader = new StreamReader(stream))
            {
                string linia;
                while ((linia = reader.ReadLine()) != null)
                {
                    linieImportu.Add(linia);
                }
            }
            _log.LogInfo("Zakończony import");
            return linieImportu;
        }
    }
}
