﻿using System;
using SOLID._3.Dobrze.Contracts;

namespace SOLID._3.Dobrze.Implementation
{
    public class ConsoleLog : ILog
    {
        public void LogError(string message, params object[] args)
        {
            Console.WriteLine(string.Concat("ERROR: ", message), args);
        }

        public void LogInfo(string message, params object[] args)
        {
            Console.WriteLine(string.Concat("INFO: ", message), args);
        }
    }
}
