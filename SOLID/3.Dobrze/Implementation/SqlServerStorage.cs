﻿using System.Collections.Generic;
using System.Linq;
using SOLID._3.Dobrze.Contracts;

namespace SOLID._3.Dobrze.Implementation
{
    public class SqlServerMagazyn : IDaneMagazyn
    {
        private readonly ILog _log;

        public SqlServerMagazyn(ILog log)
        {
            _log = log;
        }

        public void Zapisz(IEnumerable<Dane> lista)
        {
            using ( var connection =   new System.Data.SqlClient.SqlConnection(
            "Data Source=(local);Initial Catalog=Testowa;Integrated Security=True;"))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    foreach (var dane in lista)
                    {
                        var command = connection.CreateCommand();
                        command.Transaction = transaction;
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "dbo.dane";
                        command.Parameters.AddWithValue("@Identyfikator", dane.Identyfikator);
                        command.Parameters.AddWithValue("@Nazwa", dane.Nazwa);
                        command.Parameters.AddWithValue("@Opis", dane.Opis);
                        command.Parameters.AddWithValue("@Cena", dane.Cena);
                        command.Parameters.AddWithValue("@Waluta", dane.Waluta);
                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
                connection.Close();
            }

            _log.LogInfo("Zaimportowano {0} rekordów", lista.Count());

        }
    }
}
